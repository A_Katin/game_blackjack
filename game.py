import sys
import random
import check_func
from const import *


class Hand:
    def __init__(self):
        self.cards = []
        self.value = 0

    def check_blackjack(self):
        if len(self.cards) == 2 and self.get_value() == 21:
            return True
        return False

    def player_is_over(self):
        return self.get_value() > 21

    def reset(self):
        self.cards = []

    def hit(self, card):
        self.cards.append(card)

    def calculate_value(self):
        self.value = 0
        has_ace = False
        for card in self.cards:
            if card[0].isnumeric():
                self.value += int(card[0])
            else:
                if card[0] == "A":
                    has_ace = True
                    self.value += 11
                else:
                    self.value += 10

        if has_ace and self.value > 21:
            self.value -= 10

    def get_value(self):
        self.calculate_value()
        return self.value

    @staticmethod
    def print_text_card(cards):
        rank, suit = cards[0]
        smb_qt = [5, 4]
        if rank.isnumeric():
            if len(rank) == 2:
                smb_qt = [5, 3]
        text_card = f'|{rank.ljust(smb_qt[0], chr(175))}|'
        x = 1
        while x <= len(cards) - 1:
            next_rank, next_suit = cards[x]
            text_card += f'\t|{next_rank.ljust(smb_qt[0], chr(175))}|'
            if x == len(cards) - 1:
                text_card += '\n'
            x += 1

        text_card += '|' + '{:5}'.format(f' {suit}  |')
        y = 1
        while y <= len(cards) - 1:
            next_rank, next_suit = cards[y]
            text_card += ' | ' + f'{next_suit}  |'
            if y == len(cards) - 1:
                text_card += '\n'
            y += 1

        text_card += f'|{"_" * smb_qt[1]}{rank.rjust(1)}|'
        z = 1
        while z <= len(cards) - 1:
            next_rank, next_suit = cards[z]
            smb_qt = [5, 4]
            if rank.isnumeric():
                if len(next_rank) == 2:
                    smb_qt = [5, 3]
            text_card += f'\t|{"_" * smb_qt[1]}{next_rank.rjust(1)}|'
            if z == len(cards) - 1:
                text_card += '\n'
            z += 1
        print(text_card)

    def display(self, show_all_dealer_card=False):
        if isinstance(self, Dealer):
            print('\n{:_^20}\n'.format(' Дилер '))
            if not show_all_dealer_card:
                rank, suit = self.cards[1]
                smb_qt = [5, 4]
                if rank.isnumeric():
                    if len(rank) == 2:
                        smb_qt = [5, 3]
                text_card = f'|{chr(175) * (smb_qt[1] + 1)}|\t|{rank.ljust(smb_qt[0], chr(175))}|\n'
                text_card += '| ### |\t|' + '{:>6}'.format(f' {suit}  | \n')
                text_card += f'|{"_" * (smb_qt[1] + 1)}|\t|{"_" * smb_qt[1]}{rank.rjust(1)}|\n'
                print(text_card)
            else:
                self.print_text_card(self.cards)
        else:
            print('\n{:_^20}\n'.format(f' Карты {self.name} '))
            self.print_text_card(self.cards)
            print(f"Очки {self.name}: ", self.get_value())


class Dealer(Hand):
    def __init__(self):
        super().__init__()
        self.soft_stand = 17


class Player(Hand):
    _id = 0

    def __init__(self, name=None, chips=500):
        super().__init__()
        self.__class__._id += 1
        self.id = self.__class__._id
        if name is None:
            self.name = f'Игрок {self.id}'
        else:
            self.name = name
        self.chips = chips
        self.bet = 0

    def lost_bet(self):
        self.chips -= self.bet

    def wins(self, k=1):
        self.chips += self.bet * k

    def check_bet(self):
        return self.chips >= self.bet

    def check_double(self):
        return len(self.cards) == 2 and 9 <= self.get_value() <= 11 and self.chips > 0 and self.chips >= self.bet * 2

    def double_down(self):
        self.bet *= 2


class Shoe:
    def __init__(self, num_decks=6):
        self.cards = []
        for suit in (HEARTS, DIAMONDS, SPADES, CLUBS):
            for rank in range(2, 11):
                self.cards.append((str(rank), suit))
        for rank in ('J', 'Q', 'K', 'A'):
            self.cards.append((rank, suit))
        self.num_decks = num_decks
        self.new()

    def deal(self):
        if len(self.cards) > 1:
            return self.cards.pop()

    @property
    def time_to_shuffle(self):
        return len(self.cards) <= self.cut_card

    def new(self):
        self.cards *= self.num_decks
        random.shuffle(self.cards)
        if self.num_decks == 1:
            self.cut_card = len(self.cards) // 2
        else:
            self.cut_card = len(self.cards) // 4
        self.cut_card += random.randint(self.num_decks * -4, self.num_decks * 4)


class Game:
    def __init__(self, players=None, chips=500, num_decks=6):
        if players is None:
            players = [None]
        self.dealer = Dealer()
        self.shoe = Shoe(num_decks)
        self.players = [Player(name, chips) for name in players]
        self.active_players = []

    def check_shuffle(self):
        if self.shoe.time_to_shuffle:
            self.shoe.new()

    def deal_cards(self):
        for hand in self.active_players + [self.dealer]:
            hand.reset()
        for _ in range(2):
            for hand in self.active_players + [self.dealer]:
                hand.hit(self.shoe.deal())

    def collect_bets(self):
        for player in self.players.copy():
            self.get_bet(player)

    def get_bet(self, player: Player):
        if player.chips == 0:
            print(f'У {player.name} закончились фишки и он выбывает.')
            self.players.remove(player)
            return
        print(f'\nУ {player.name} - {player.chips} фишек.')
        bet = check_func.get_int(f'Сделайте вашу ставку? (1-{player.chips}) или [q]Выход: ', 1, player.chips, 'q')
        if bet == 'q':
            print(f'{player.name} вышел!')
            self.players.remove(player)
            return
        player.bet = bet
        if bet > 0:
            self.active_players.append(player)

    def print_hands(self):
        self.dealer.display()
        for player in self.active_players:
            player.display()

    def check_dealer_blackjack(self):
        if self.dealer.check_blackjack():
            self.dealer.display(True)
            self.resolve_dealer_blackjack()
            return True
        return False

    def resolve_dealer_blackjack(self):
        print('У дилера выпал BlackJack!')
        for player in self.active_players:
            if player.check_blackjack():
                print(f'{BRAVO} У {player.name} тоже выпал BlackJack! {SMILE}')
                player.wins()
            else:
                player.lost_bet()
        self.active_players.clear()

    def check_player_blackjacks(self):
        for player in self.active_players.copy():
            if player.check_blackjack():
                print(f'{BRAVO} У {player.name} выпал BlackJack!')
                player.wins(1.5)
                self.active_players.remove(player)

    def play_single_hand(self, player: Player):
        hand = player
        while not hand.player_is_over():
            action_list = [HIT, STICK]
            if hand.check_double():
                if hand.check_bet():
                    action_list.append(DOUBLE)
            choice = check_func.get_str(f'\n{hand.name} cделайте ваш выбор ' + ', '.join(action_list) + '> ')
            while choice not in ['h', 's', 'H', 'S', 'd', 'D']:
                choice = check_func.get_str(f'\n{hand.name} cделайте ваш выбор ' + ', '.join(action_list) + '> ')
            if choice in ['H', 'h', 'd', 'D']:
                if choice in ['d', 'D']:
                    hand.double_down()
                    print('\n{:_^25}'.format(f'{hand.name} Ваша ставка увеличена до {hand.bet} '))
                hand.hit(self.shoe.deal())
                hand.display()
                if hand.get_value() == 21:
                    break
            else:
                break  # переход хода
        else:
            print(f'\nПеребор, {hand.name} проиграл!  {SAD}')
            hand.lost_bet()
            self.active_players.remove(player)

    def pay_winners(self):
        print('\n{:_^30}'.format(f' Финальные результаты '))
        print('{:*^30}'.format('*'))
        print('{:30}'.format(f'Очки дилера: {self.dealer.get_value()}'))
        for player in self.active_players:
            print('{:30}'.format(f'Очки {player.name}: {player.get_value()}'))
            self.resolve_hand(player)

    def resolve_hand(self, player):
        if player.player_is_over():
            return
        elif (self.dealer.player_is_over()
              or player.get_value() > self.dealer.get_value()):
            player.wins(2)
            print(f'Поздравляем! {HLOPS} {player.name} выиграл! {SMILE}!')
        elif player.get_value() == self.dealer.get_value():
            print(f'У {player.name} ничья, ставка возвращается.')
        else:
            player.lost_bet()
            print(f'{player.name} проиграл! {SAD}.')

    def mainloop(self):
        while self.play_hand():
            pass

    def play_hand(self):
        self.check_shuffle()
        self.active_players = []
        self.collect_bets()
        if not self.players:
            return False
        self.deal_cards()
        self.print_hands()
        if self.check_dealer_blackjack():
            return True
        self.check_player_blackjacks()
        for player in self.active_players.copy():
            self.play_single_hand(player)

        if self.active_players:
            print('\nДилер ходит...')
            while True:
                if self.dealer.get_value() and self.dealer.get_value() >= self.dealer.soft_stand:
                    break
                self.dealer.hit(self.shoe.deal())
            self.dealer.display(True)
            self.pay_winners()
        else:
            self.dealer.display(True)
        return True


def setup():
    num_players = check_func.get_int('Введите количество игроков [1-6] или [q]для быстрого старта: ', 1, 6, 'q')
    if num_players == 'q':
        return Game()
    players = []
    for n in range(num_players):
        name = check_func.get_str(f'Введите имя игрока {n}: ')
        players.append(name)

    chips = check_func.get_int('\nВведите сумму фишек у игроков: ', min=1)
    decks = check_func.get_int('\nВведите количество колод [1-8] в шузе: ', 1, 8)
    return Game(players, chips, decks)


def main():
    print('\nWelcome to PyBlackjack!')
    game = setup()
    game.mainloop()
    print('Спасибо за игру! / Thanks for playing!')
    return 0


if __name__ == '__main__':
    sys.exit(main())
